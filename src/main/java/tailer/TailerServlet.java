package tailer;

import org.apache.commons.io.input.ReversedLinesFileReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@WebServlet("/*")
public class TailerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (PrintWriter out = resp.getWriter()) {
            resp.setContentType("text/plain");
            try {
                String filename = req.getPathInfo();
                int limit = getParam(req, "n", 500);

                File file = new File(filename);
                if (file.isDirectory()) {
                    List<String> list = new ArrayList<>(Arrays.asList(file.list()));
                    Collections.sort(list);
                    for (String name : list) {
                        out.println(name);
                    }
                } else {
                    ReversedLinesFileReader reader = new ReversedLinesFileReader(file, StandardCharsets.UTF_8);
                    for (int i = 0; i < limit; i++) {
                        String line = reader.readLine();
                        if (line == null) {
                            break;
                        }
                        out.println(line);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace(out);
            }
        }
    }

    private int getParam(HttpServletRequest req, String name, int def) {
        try {
            return Integer.parseInt(req.getParameter(name));
        } catch (Exception ex) {
            return def;
        }
    }

    private String getParam(HttpServletRequest req, String name, String def) {
        String value = req.getParameter(name);
        return value == null ? def : value;
    }
}